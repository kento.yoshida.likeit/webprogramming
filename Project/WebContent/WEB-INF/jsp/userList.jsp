<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー一覧画面</title>
<link href="css/original/userList.css" rel="stylesheet">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

</head>
<body>
   <div class="header" >
     <h3>ユーザー管理システム</h3>
     <a href="LogoutServlet" >ログアウト</a>
     <h4>${userInfo.name} さん</h4>
   </div>
   <div class="sBox" >
    <form action="UserAddServlet"  method="get">
     <input class=shinki type="submit" value="新規登録">
    </form>
   </div>
   <form action="UserListServlet" method="post">
    <div class="searchBox" align="center">
     <ul>
       <li>ログインID<input class="box" type="text" name="loginId"></li>
       <li>ユーザー名<input class="box" type="text" name="name"></li>
       <li>生年月日 <input class="bbox" type="date" name="birthDate1">～<input class="bbox" type="date" name="birthDate2"></li>
     </ul>
     <input class="search" type="submit" value="検索" >
    </div>
   </form>
   <div align="center">
    <table>
      <thead>
        <tr class="footer1">
	     <th>ログイン</th>
	     <th>ユーザー名</th>
	      <th>生年月日</th>
	      <th></th>
	     </tr>
	    </thead>
			<tbody>
				<c:forEach var="user" items="${userList}">
					<tr>
						<td>${user.loginId}</td>
						<td>${user.name}</td>
						<td>${user.birthDate}</td>
						<td>
						  <a href="UserDetailServlet?loginId=${user.loginId}">
				           <input class="detail" type="submit" value="詳細" >
				          </a>
				          <c:if test="${userInfo.loginId == 'admin' or userInfo.loginId == user.loginId}">
				          <a href="UserUpdateServlet?loginId=${user.loginId}">
				           <input class="update" type="submit" value="更新" >
				          </a>
				          </c:if>
				          <c:if test="${userInfo.loginId == 'admin'}" >
				           <a href="UserDeleteServlet?loginId=${user.loginId}">
				            <input class="delete" type="submit" value="削除" >
				           </a>
				          </c:if>
						</td>
					</tr>
				</c:forEach>
			</tbody>
    </table>
   </div>
</body>
</html>