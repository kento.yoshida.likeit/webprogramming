<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー情報詳細参照</title>
<link href="css/original/userDetail.css" rel="stylesheet">
</head>
<body>
  <div class="header">
     <h3>${userInfo.name} さん</h3>
     <a style=float:right;padding-right:5px;padding-top:10px;color:#dfdfdf; href="LogoutServlet" class="logout">ログアウト</a>
   </div>
  <div class="main" align="center">
    <h1>ユーザー情報詳細参照</h1>
     <table>
     	<tr>
     		<td>
     			ログインID
     		</td>
     		<td>
     		</td>
     		<td>
     			${wantedUser.loginId}
     		</td>
     	</tr>
     	<tr>
     		<td>
     			ユーザー名
     		</td>
     		<td>
     		</td>
     		<td>
     			${wantedUser.name}
     		</td>
     	</tr>
     	<tr>
     		<td>
     			生年月日
     		</td>
     		<td>
     		</td>
     		<td>
     			${wantedUser.birthDate}
     		</td>
     	</tr>
     	     	<tr>
     		<td>
     			登録日時
     		</td>
     		<td>
     		</td>
     		<td>
     			${wantedUser.createDate}
     		</td>
     	</tr>
     	     	<tr>
     		<td>
     			更新日時
     		</td>
     		<td>
     		</td>
     		<td>
     			${wantedUser.updateDate}
     		</td>
       </tr>
     </table>
  </div>
  <a href="UserListServlet">戻る</a>
</body>
</html>