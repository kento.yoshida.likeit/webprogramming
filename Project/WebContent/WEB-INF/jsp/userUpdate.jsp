<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー情報更新</title>
<link href="css/original/userUpdate.css" rel="stylesheet">
</head>
<body>
  <div class="header">
     <h3>${userInfo.name} さん</h3>
      <a style=float:right;padding-right:5px;padding-top:10px;color:#dfdfdf; href="LogoutServlet" class="logout">ログアウト</a>
   </div>
  <div class="main" align="center">
   <form action="UserUpdateServlet" method="post">
    <h1>ユーザー情報更新</h1>
     <table>
     	<tr>
     		<td>
     			ログインID
     		</td>
     		<td>
     		</td>
     		<td>
     			${wantedUser.loginId}
     		</td>
     	</tr>
     	<tr>
     		<td>
     			パスワード
     		</td>
     		<td>
     		</td>
     		<td>
     		     <input class="box" type="text" name="password">
     		</td>
     	</tr>
     	<tr>
     		<td>
     			パスワード(確認)
     		</td>
     		<td>
     		</td>
     		<td>
     			<input class="box" type="text" name="rePassword">
     		</td>
     	</tr>
     	     	<tr>
     		<td>
     			ユーザー名
     		</td>
     		<td>
     		</td>
     		<td>
     			<input class="box" type="text" name="name" value="${wantedUser.name}">
     		</td>
     	</tr>
     	     	<tr>
     		<td>
     			生年月日
     		</td>
     		<td>
     		</td>
     		<td>
     		    <input class="box" type="date" name="birthDate" value="${wantedUser.birthDate}">
     		</td>
     	</tr>
     </table>
      <input type="submit" value="更新">
    </form>
  </div>
  <a href="UserListServlet">戻る</a>
</body>
</html>