<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー削除確認</title>
</head>
<link href="css/original/userDelete.css" rel="stylesheet">
<body>
<div class="header">
     <h3>${userInfo.name} さん</h3>
     <a style=float:right;padding-right:5px;padding-top:10px;color:#dfdfdf; href="LogoutServlet" class="logout">ログアウト</a>
</div>
<div class="main" align="center">
    <h1>ユーザー削除確認</h1>
</div>
<table>
      <tr>
         <td>
             ログインID:
         </td>
         <td>
             ${loginId}
         </td>
       </tr>
</table>
<p>を本当に削除してよろしいでしょうか。</p>
<div class="button" align="center">
 <form style="display:inline" action="UserListServlet" method="get">
  <input type="submit" value="キャンセル">
 </form>
 <form style="display:inline" action="UserDeleteServlet" method="post">
  <input type="submit" value="OK">
 </form>
</div>
</body>
</html>