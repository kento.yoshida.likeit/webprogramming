<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー新規登録</title>
<link href="css/original/userAdd.css" rel="stylesheet">
</head>
<body>
  <div class="header">
     <h3>${userInfo.name} さん</h3>
     <a style=float:right;padding-right:5px;padding-top:10px;color:#dfdfdf; href="LogoutServlet" class="logout">ログアウト</a>
   </div>
  <div class="main" align="center">
    <h1>ユーザー新規登録</h1>
    <div style=color:red;>
     ${errMsg}
    </div>
    <form action="UserAddServlet" method="post">
     <table>
       <tr>
         <td>
           ログインID
         </td>
         <td>
           <input class="box" type="text" name="loginId">
         </td>
       </tr>
       <tr>
         <td>
           パスワード
         </td>
         <td>
           <input class="box" type="text" name="password">
         </td>
       </tr>
       <tr>
         <td>
           パスワード(確認)
         </td>
         <td>
           <input class="box" type="text" name="rePassword">
         </td>
       </tr>
       <tr>
         <td>
           ユーザー名
         </td>
         <td>
           <input class="box" type="text" name="name">
         </td>
       </tr>
       <tr>
         <td>
           生年月日
         </td>
         <td>
           <input class="box" type="date" name="birthDate">
         </td>
       </tr>
     </table>
       <input type="submit" value="登録">
   </form>
  </div>
  <a href="UserListServlet">戻る</a>
</body>
</html>