<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="UTF-8">
<title>ログイン画面</title>
 <link href="css/original/login.css" rel="stylesheet">
 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
</head>
<body>
 <form action="LoginServlet" method="post">
  <div class=login  align="center">
    <h1>ログイン画面</h1>
    <c:if test="${errMsg != null}" >
	    <div style=color:red; class="alert" role="alert">
		  ${errMsg}
		</div>
	</c:if>
	<input type="text"  name="loginId" placeholder="ログインID" onfocus="this.style.border='1px solid #00a1e9'">
	<p></p>
	<input type="password" name="password" placeholder="Password" onfocus="this.style.border='1px solid #00a1e9'">
	<p></p>
	<input class=b type="submit" value="ログイン">
   </div>
  </form>
</body>
</html>