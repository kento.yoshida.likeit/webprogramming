package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.UserBeans;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ログインセッションがない場合、ログイン画面にリダイレクトさせる
		HttpSession session = request.getSession();
		UserBeans user= (UserBeans)session.getAttribute("userInfo");
		if(user==null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		// URLからGETパラメータとしてloginIdを受け取る
		String loginId = request.getParameter("loginId");

		//loginIdをセッションスコープに入れる
		session.setAttribute("loginId", loginId);

		//クリックした更新ボタンに紐づいたloginIdに紐づいたユーザー情報を取得
		UserDao userDao = new UserDao();
		UserBeans wantedUser = userDao.wantedUserDetailInfo(loginId);

		//wantedUserをリクエストスコープにセット
		request.setAttribute("wantedUser", wantedUser);

		//UserUpdate.jspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// リクエストパラメータの文字コードを指定
        request.setCharacterEncoding("UTF-8");

		// セッションスコープのloginIdを取得
        HttpSession session = request.getSession();
        String loginId = (String)session.getAttribute("loginId");

        // セッションスコープのloginIdを破棄
        session.removeAttribute("loginId");

        // リクエストパラメータの入力項目を取得
		String password = request.getParameter("password");
		String rePassword = request.getParameter("rePassword");
		String name = request.getParameter("name");
		String birthDate =request.getParameter("birthDate");

		if(!password.equals(rePassword)||name.equals("")||birthDate.equals("")) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			// userAdd.jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userAdd.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//sql更新回数を格納するresult
		int result = 0;

		//PassとrePassが空白の場合と全て揃ってる場合のif分岐
		UserDao userDao = new UserDao();

		if(password.equals("")&&rePassword.equals("")) {
			result = userDao.userUpdateNoPassword(loginId,name,birthDate);

		}else {
			result = userDao.userUpdate(loginId,password,name,birthDate);
		}

		if(result==0) {
			// リクエストスコープにエラーメッセージをセット
			request.setAttribute("errMsg", "入力された内容は正しくありません");

			// userUpdate.jspにフォワード
			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);

		}else if(result==1){
			//UserListServletにリダイレクト
			response.sendRedirect("UserListServlet");
		}
	}

}
