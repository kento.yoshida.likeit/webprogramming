package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.UserDao;
import model.UserBeans;

/**
 * Servlet implementation class UserDeleteServlet
 */
@WebServlet("/UserDeleteServlet")
public class UserDeleteServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDeleteServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//ログインセッションがない場合、ログイン画面にリダイレクトさせる
		HttpSession session = request.getSession();
		UserBeans user= (UserBeans)session.getAttribute("userInfo");
		if(user==null) {
			response.sendRedirect("LoginServlet");
			return;
		}

		// URLからGETパラメータとしてloginIdを受け取る
		String loginId = request.getParameter("loginId");

		//loginIdをセッションスコープに入れる
		session.setAttribute("loginId", loginId);

		//UserDelete.jspにフォワード
		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userDelete.jsp");
		dispatcher.forward(request, response);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//セッションスコープからloginIdを取り出す
		 HttpSession session = request.getSession();
	     String loginId = (String)session.getAttribute("loginId");

		//セッションスコープのloginIdを破棄
	     session.removeAttribute("loginId");

	   //sql更新回数を格納するresult
		int result = 0;

	   //userDaoのuserDeleteメソッドを呼び出す
		UserDao userDao = new UserDao();
		result = userDao.userDelete(loginId);

		//成功判定
		 if(result==1){
		//UserListServletにリダイレクト
		response.sendRedirect("UserListServlet");
		}
	}

}
