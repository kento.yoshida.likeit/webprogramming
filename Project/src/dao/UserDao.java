package dao;

import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.UserBeans;

/**
 * ユーザテーブル用のDao
 * @author yoshida
 *
 */
public class UserDao {

    /**
     * ログインIDとパスワードに紐づくユーザ情報を返す
     * @param loginId
     * @param password
     * @return
     */
    public UserBeans findByLoginInfo(String loginId, String password) {
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginId);
           //パスワードを暗号化
			String encryptedPass = passEncrypt(password);
            pStmt.setString(2, encryptedPass);
            ResultSet rs = pStmt.executeQuery();


            // ログイン失敗時の処理
            if (!rs.next()) {
                return null;
            }

            // ログイン成功時の処理
            String loginIdData = rs.getString("login_id");
            String nameData = rs.getString("name");
            return new UserBeans(loginIdData, nameData);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }

    public UserBeans wantedUserDetailInfo(String loginId) {
    	Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM user WHERE login_id = ?";

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginId);
            ResultSet rs = pStmt.executeQuery();
            rs.next();

            // 結果表からUserBeansコンストラクタに詰める全カラム(データを)取得
            int id = rs.getInt("id");
            String loginIdData = rs.getString("login_id");
            String nameData = rs.getString("name");
            Date birthDateData = rs.getDate("birth_date");
            String passwordData = rs.getString("password");
            String createDate = rs.getString("create_date");
            String updateDate = rs.getString("update_date");


            return new UserBeans(id,loginIdData,nameData,birthDateData,passwordData,createDate,updateDate);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }
    /**
     * 全てのユーザ情報を取得する
     * @return
     */
    public List<UserBeans> findAll() {
        Connection conn = null;
        List<UserBeans> userList = new ArrayList<UserBeans>();

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            // TODO: 実装済：管理者以外を取得するようSQLを変更する
            String sql = "SELECT * FROM user WHERE id!=1 ";

             // SELECTを実行し、結果表を取得
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            // 結果表に格納されたレコードの内容を
            // Userインスタンスに設定し、ArrayListインスタンスに追加
            while (rs.next()) {
                int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String name = rs.getString("name");
                Date birthDate = rs.getDate("birth_date");
                String password = rs.getString("password");
                String createDate = rs.getString("create_date");
                String updateDate = rs.getString("update_date");
                UserBeans user = new UserBeans(id, loginId, name, birthDate, password, createDate, updateDate);

                userList.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return userList;
    }
    //新規登録
    public int userInsert(String loginId,String name,String birthDate, String password) {
    	Connection con = null;
		PreparedStatement stmt = null;
		int result = 0;
		try {
			// データベース接続
			con = DBManager.getConnection();
			// 実行SQL文字列定義
			String insertSQL = "INSERT INTO user VALUES(0,?,?,?,?,now(),now());";
			// ステートメント生成
			stmt = con.prepareStatement(insertSQL);
			// SQLの?パラメータに値を設定
			stmt.setString(1, loginId);
			stmt.setString(2, name);
			stmt.setString(3, birthDate);
			//パスワードを暗号化
			String encryptedPass = passEncrypt(password);
			stmt.setString(4, encryptedPass);
			// 登録SQL実行
			result = stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	    return result;

    }
    //ユーザー情報更新全てあり
    public int userUpdate(String loginId,String password,String name,String birthDate) {
    	Connection con = null;
		PreparedStatement stmt = null;
		int result = 0;
		try {
			// データベース接続
			con = DBManager.getConnection();
			// 実行SQL文字列定義
			String updateSQL = "UPDATE user SET name = ?, birth_date = ?,password = ?,update_date=now() WHERE login_id = ?";
			// ステートメント生成
			stmt = con.prepareStatement(updateSQL);
			// SQLの?パラメータに値を設定
			stmt.setString(1, name);
			stmt.setString(2, birthDate);
			//パスワードを暗号化
			String encryptedPass = passEncrypt(password);
			stmt.setString(3, encryptedPass);
			stmt.setString(4, loginId);
			// 登録SQL実行
			result = stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	    return result;

    }

    //ユーザー情報更新Pass無し
    public int userUpdateNoPassword(String loginId,String name,String birthDate) {
    	Connection con = null;
		PreparedStatement stmt = null;
		int result = 0;
		try {
			// データベース接続
			con = DBManager.getConnection();
			// 実行SQL文字列定義
			String updateSQL = "UPDATE user SET name = ?, birth_date = ?, update_date = now() WHERE login_id = ?";
			// ステートメント生成
			stmt = con.prepareStatement(updateSQL);
			// SQLの?パラメータに値を設定
			stmt.setString(1, name);
			stmt.setString(2, birthDate);
			stmt.setString(3, loginId);
			// 登録SQL実行
			result = stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	    return result;

    }

    public int userDelete(String loginId) {
    	Connection con = null;
		PreparedStatement stmt = null;
		int result = 0;
		try {
			// データベース接続
			con = DBManager.getConnection();
			// 実行SQL文字列定義
			String deleteSQL = "DELETE FROM user WHERE login_id = ?";
			// ステートメント生成
			stmt = con.prepareStatement(deleteSQL);
			// SQLの?パラメータに値を設定
			stmt.setString(1, loginId);
			// 登録SQL実行
			result = stmt.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			try {
				// ステートメントインスタンスがnullでない場合、クローズ処理を実行
				if (stmt != null) {
					stmt.close();
				}
				// コネクションインスタンスがnullでない場合、クローズ処理を実行
				if (con != null) {
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}

	    return result;
    }

    public List<UserBeans> search (String searchLoginId,String searchName,String birthDate1,String birthDate2) {

    	Connection conn = null;
        List<UserBeans> userList = new ArrayList<UserBeans>();

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備

            String sql = "SELECT * FROM user WHERE id!=1 ";

            if(!searchLoginId.equals("")) {
            	sql += " AND login_id = '"+searchLoginId+"'";
            }if(!searchName.equals("")) {
            	sql += " AND name LIKE '%"+searchName+"%'";
            }if(!birthDate1.equals("")) {
            	sql += " AND birth_date >= '"+birthDate1+"'";
            }if(!birthDate2.equals("")) {
            	sql += " AND birth_date <= '"+birthDate2+"'";
            }

             // SELECTを実行し、結果表を取得
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            // 結果表に格納されたレコードの内容を
            // Userインスタンスに設定し、ArrayListインスタンスに追加
            while (rs.next()) {
                int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String name = rs.getString("name");
                Date birthDate = rs.getDate("birth_date");
                String password = rs.getString("password");
                String createDate = rs.getString("create_date");
                String updateDate = rs.getString("update_date");
                UserBeans user = new UserBeans(id, loginId, name, birthDate, password, createDate, updateDate);

                userList.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return userList;

    }

    public String passEncrypt(String password) {
    	//ハッシュを生成したい元の文字列
    	String source = password;
    	//ハッシュ生成前にバイト配列に置き換える際のCharset
    	Charset charset = StandardCharsets.UTF_8;
    	//ハッシュアルゴリズム
    	String algorithm = "MD5";
    	//ハッシュ生成処理
    	byte[] bytes=null;

		try {
			bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));

		} catch (NoSuchAlgorithmException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
		}
		String result = DatatypeConverter.printHexBinary(bytes);

    	//標準出力
    	System.out.println(result);

    	return result;
    }
}

